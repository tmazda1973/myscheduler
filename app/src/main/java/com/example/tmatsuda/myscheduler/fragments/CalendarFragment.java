package com.example.tmatsuda.myscheduler.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.Toast;

import com.example.tmatsuda.myscheduler.R;
import com.example.tmatsuda.myscheduler.common.utils.DateUtils;
import com.example.tmatsuda.myscheduler.listener.OnCalenderDateChangeListener;

/**
 * カレンダー要素の描画を行うフラグメントクラスです。
 * @author t.matsuda
 */
public class CalendarFragment extends Fragment implements CalendarView.OnDateChangeListener {


///// method

	/**
	 * コンストラクタ
	 */
	public CalendarFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		/* 要素を初期化する */
		View view = inflater.inflate(R.layout.fragment_calender, container, false);
		// カレンダービュー
		CalendarView calendarView = (CalendarView)view.findViewById(R.id.calendarView);
		calendarView.setOnDateChangeListener(this);

		// 処理終了
		return view;
	}

///// listener

	/**
	 * カレンダービューの日付が変更された時に呼び出されます。
	 * @param view カレンダービュー
	 * @param year 変更された日付：年
	 * @param month 変更された日付：月
	 * @param date 変更された日付：日
	 */
	@Override
	public void onSelectedDayChange(CalendarView view, int year, int month, int date) {
		// カレンダーデータを日付文字列に変換する
		String changeDate = DateUtils.calendarToDateFormat(year, month, date);
		// TODO: デバッグ用
		Toast.makeText(getActivity().getApplicationContext(), changeDate, Toast.LENGTH_SHORT).show();
		/* 親アクティビティにデータを受け渡す */
		Activity parent = this.getActivity();
		// 親アクティビティがイベントリスナーを実装している場合
		if (parent instanceof OnCalenderDateChangeListener) {
			// 親アクティビティに実装しているイベントハンドラを実行する
			OnCalenderDateChangeListener listener = (OnCalenderDateChangeListener)parent;
			listener.onDateChanged(changeDate);
		}
	}
}