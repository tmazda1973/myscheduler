package com.example.tmatsuda.myscheduler.application;

import android.app.Application;

import com.example.tmatsuda.myscheduler.greendao.DaoSession;
import com.example.tmatsuda.myscheduler.helper.db.GreenDaoDbHelper;

/**
 * @author t.matsuda
 */
public class MyApplication extends Application {
	private static final String TAG = MyApplication.class.getSimpleName();

	/**
	 * 初期化メソッド
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		// GreenDaoHelperを使用可能にする
		GreenDaoDbHelper.getInstance(this, null);
	}

	/**
	 * DB操作ヘルパーのインスタンスを取得します。
	 * @return DB操作ヘルパーのインスタンス
	 */
	public GreenDaoDbHelper getSQLOpenHelper() {
		return GreenDaoDbHelper.getInstance();
	}
	/**
	 * GreenDAOセッションオブジェクトのインスタンスを取得します。
	 * @return セッションオブジェクトのインスタンス
	 */
	public DaoSession getDaoSession() {
		return GreenDaoDbHelper.getInstance().getDaoSession();
	}
}
