package com.example.tmatsuda.myscheduler.model;

import android.app.Activity;

import com.example.tmatsuda.myscheduler.application.MyApplication;
import com.example.tmatsuda.myscheduler.common.utils.StringUtils;
import com.example.tmatsuda.myscheduler.dto.TScheduleDto;
import com.example.tmatsuda.myscheduler.greendao.DaoSession;
import com.example.tmatsuda.myscheduler.greendao.TSchedule;
import com.example.tmatsuda.myscheduler.greendao.TScheduleDao;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.dao.query.QueryBuilder;

/**
 * スケジュール情報のモデルクラスです。<br />
 * - MVCモデルのModel層の処理を行います。
 * @author thinkware
 */
public class ScheduleModel {

///// field

	/** スケジュール情報DAOクラス */
	private TScheduleDao mTScheduleDao = null;

///// method

	/**
	 * コンストラクタ
	 * @param activity アクティビティ
	 */
	public ScheduleModel(Activity activity) {
		/*** クラス生成時の初期処理を行う ***/
		MyApplication application = (MyApplication)activity.getApplicationContext();
		DaoSession session = application.getDaoSession();
		// DAOクラスを生成する
		this.mTScheduleDao = session.getTScheduleDao();
	}

	/**
	 * スケジュール情報を抽出します。
	 * @param id データID
	 * @return 抽出データ
	 */
	public TScheduleDto find(Long id) {
		TSchedule entity = this.mTScheduleDao.load(id);
		return this.__copyProperties(entity);
	}

	/**
	 * 基準日以前のスケジュール情報を抽出します。
	 * @param date 基準日
	 * @return 抽出データリスト
	 */
	public List<TScheduleDto> find(String date) {
		QueryBuilder queryBuilder = this.mTScheduleDao.queryBuilder();
		if (!StringUtils.isEmpty(date)) {
			queryBuilder = queryBuilder.where(TScheduleDao.Properties.Date.le(date));
		}
		queryBuilder.orderDesc(TScheduleDao.Properties.Date, TScheduleDao.Properties.Id);
		List<TSchedule> entityList = queryBuilder.list();
		return this.__copyProperties(entityList);
	}

	/**
	 * スケジュール情報を追加・更新します。
	 * @param entity 追加・更新データ
	 * @return データID
	 */
	public Long insertOrReplace(TSchedule entity) {
		return this.mTScheduleDao.insertOrReplace(entity);
	}

	/**
	 * スケジュール情報を削除します。
	 * @param entity 削除データ
	 */
	public void delete(TSchedule entity) {
		this.mTScheduleDao.delete(entity);
	}

	/**
	 * スケジュール情報を削除します。
	 * @param id プライマリキー
	 */
	public void deleteById(Long id) {
		this.mTScheduleDao.deleteByKey(id);
	}

///// private method

	/**
	 * データオブジェクトのプロパティを複製します。
	 * @param src 複製元データ
	 * @return 複製先データ
	 */
	private TScheduleDto __copyProperties(TSchedule src) {
		TScheduleDto dto = new TScheduleDto();
		dto.setId(src.getId());
		dto.setDate(src.getDate());
		dto.setTitle(src.getTitle());
		dto.setContent(src.getContent());
		dto.setCreated(src.getCreated());
		dto.setModified(src.getModified());
		return dto;
	}

	/**
	 * データオブジェクトのプロパティを複製します。
	 * @param srcList 複製元データリスト
	 * @return 複製先データリスト
	 */
	private List<TScheduleDto> __copyProperties(List<TSchedule> srcList) {
		List<TScheduleDto> destList = new ArrayList<>();
		for (TSchedule src : srcList) {
			TScheduleDto dto = this.__copyProperties(src);
			destList.add(dto);
		}
		return destList;
	}
}