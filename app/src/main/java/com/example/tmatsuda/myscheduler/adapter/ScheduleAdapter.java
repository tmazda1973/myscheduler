package com.example.tmatsuda.myscheduler.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.tmatsuda.myscheduler.R;
import com.example.tmatsuda.myscheduler.dto.TScheduleDto;

import java.util.List;

/**
 * スケジュール一覧のアダプタークラスです。
 * @author t.matsuda
 */
public class ScheduleAdapter extends ArrayAdapter<TScheduleDto> {

	private LayoutInflater mLayoutInflater = null;

	/**
	 * コンストラクタ
	 * @param context
	 * @param resourceId
	 * @param objects
	 */
	public ScheduleAdapter(Context context, int resourceId, List<TScheduleDto> objects) {
		super(context, resourceId, objects);
		this.mLayoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	/**
	 * リストの行描画時に呼び出されます。
	 * @param position
	 * @param convertView
	 * @param parent
	 * @return リスト行のビューオブジェクト
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder; // ビューホルダー
		// 特定行(position)のデータを得る
		TScheduleDto item = getItem(position);
		// ビューが再利用されていない場合
		if (null == convertView) {
			/* ビューを新規に生成する */
			convertView = this.mLayoutInflater.inflate(R.layout.schedule_item, parent, false);
			holder = new ViewHolder();
			holder.txtDate    = (TextView)convertView.findViewById(R.id.txt_schedule_date);
			holder.txtTitle   = (TextView)convertView.findViewById(R.id.txt_title);
			holder.txtContent = (TextView)convertView.findViewById(R.id.txt_content);
			convertView.setTag(holder);
		}
		// ビューが再利用されている場合
		else {
			/* ビューを初期化する */
			holder = (ViewHolder)convertView.getTag();
			holder.txtDate.setText(null);
			holder.txtTitle.setText(null);
			holder.txtContent.setText(null);
        }
        /* 画面コンポーネントにデータを反映する */
        // スケジュール日付
        String scheduleDate = item.getDate();
		holder.txtDate.setText(scheduleDate);
        // タイトル
		holder.txtTitle.setText(item.getTitle());
        // 内容
		holder.txtContent.setText(item.getContent());

		// 処理終了
		return convertView;
	}

///// inner class

	/**
	 * ビューホルダークラスです。
	 * @author t.matsuda
	 */
	private class ViewHolder {
		TextView txtDate;
		TextView txtTitle;
		TextView txtContent;
	}
}
