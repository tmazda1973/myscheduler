package com.example.tmatsuda.myscheduler.listener;

/**
 * カレンダービューの日付が変更されたことを通知するリスナークラスです。
 * @author t.matsuda
 */
public interface OnCalenderDateChangeListener {
	void onDateChanged(String date);
}
