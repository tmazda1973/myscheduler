package com.example.tmatsuda.myscheduler.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * DBレコードのDAOインターフェースクラスです。
 * @author t.matsuda
 */
public interface BaseDao<K extends Serializable, T> {
	public T find(K id);
	public List<T> find(Map<String, Object> condition, Map<String, Object> order);
	public K insert(T value);
	public void update(T value);
	public void delete(T value);
}
