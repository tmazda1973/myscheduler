package com.example.tmatsuda.myscheduler.activities;

import android.os.Bundle;

import com.example.tmatsuda.myscheduler.R;

/**
 * スケジュール情報一覧画面のアクティビティクラスです。
 * @author t.matsuda
 */
public class ScheduleItemsActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_schedule_items);
	}
}
