package com.example.tmatsuda.myscheduler.activities;

import android.os.Bundle;

import com.example.tmatsuda.myscheduler.R;

/**
 * スケジュール詳細情報画面のアクティビティクラスです。
 * @author t.matsuda
 */
public class ScheduleDetailActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_schedule_detail);
	}
}
