package com.example.tmatsuda.myscheduler.common.utils;

import android.support.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * 日付に関するメソッドを提供するユーティリティクラスです。
 * @author t.matsuda
 */
public class DateUtils {

///// property

	/** ログ出力用のタグ名 */
	private static final String TAG = DateUtils.class.getSimpleName();

	public static final String FORMAT_YYYYMMDD_HYPHEN = "yyyy-MM-dd";
	public static final String FORMAT_YYYYMMDD_SLASH = "yyyy/MM/dd";
	public static final String FORMAT_YYYYMMDDH24MMSS_HYPHEN = "yyyy-MM-dd HH:mm:ss";
	public static final String FORMAT_YYYYMMDDH24MMSS_SLASH = "yyyy/MM/dd HH:mm:ss";
	public static final String FORMAT_YYYYMMDDH12MMSS_SLASH = "yyyy/MM/dd hh:mm:ss";

///// method

	/**
	 * コンストラクタ
	 * - インスタンス化を禁止します。
	 */
	private DateUtils() {}

	/**
	 * 現在日時を指定された形式の文字列に変換します。
	 * @param format 日付フォーマット
	 * @return 日付文字列
	 */
	@NonNull
	public static String toDateFormat(String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(Calendar.getInstance().getTime());
	}

	/**
	 * カレンダーの年・月・日をSQLデータ形式の日付文字列に変換します。
	 * @param year 年
	 * @param month 月
	 * @param date 日
	 * @return 日付文字列（yyyy-MM-dd）
	 */
	@NonNull
	public static String calendarToDateFormat(int year, int month, int date) {
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_YYYYMMDD_HYPHEN);
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, date);
		return sdf.format(calendar.getTime());
	}
}
