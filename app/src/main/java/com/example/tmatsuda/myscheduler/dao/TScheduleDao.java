package com.example.tmatsuda.myscheduler.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.tmatsuda.myscheduler.common.utils.SQLUtils;
import com.example.tmatsuda.myscheduler.dto.TScheduleDto;
import com.example.tmatsuda.myscheduler.helper.db.DatabaseHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * DBレコードのDAO実装クラスです。(スケジュールテーブル)
 * @author thinkware
 */
public class TScheduleDao implements BaseDao<Long, TScheduleDto> {

///// field

	/** DB制御オブジェクト */
	private SQLiteDatabase db = null;

///// method

	/**
	 * コンストラクタ
	 * @param context コンテキストオブジェクト
	 */
	public TScheduleDao(Context context) {
		// DB制御オブジェクトのインスタンスを取得する
		this.db = DatabaseHelper.getInstance(context).getWritableDatabase();
	}

	/**
	 * DBにレコードを追加します。
	 * @param dto データオブジェクト
	 * @return 行ID
	 */
	public Long insert(TScheduleDto dto) {
		// DBにレコードを追加する
		Long rowId = this.db.insertOrThrow(
			TScheduleDto.TABLE_NAME, null, this.__toContentValues(dto)
		);
		return rowId;
	}

	/**
	 * DBのレコードを更新します。
	 * @param dto データオブジェクト
	 * @return なし
	 */
	public void update(TScheduleDto dto) {
		//******************************************************************************************
		//* DBのレコードを更新する
		//******************************************************************************************
		// レコードの項目値を設定する
		ContentValues values = this.__toContentValues(dto);
		// 抽出条件を生成する
		StringBuilder conditionBuilder = new StringBuilder();
		conditionBuilder.append(TScheduleDto.COLUMN_ID + "=?");
		//*------------------------------------------------------------------------------------*
		//* DBのレコードを更新する
		//*------------------------------------------------------------------------------------*
 		this.db.update(
				TScheduleDto.TABLE_NAME, values, conditionBuilder.toString(),
				new String[]{
						String.valueOf(dto.getId())
				});

		// 処理終了
		return;
	}

	/**
	 * DBのレコードを削除します。
	 * @param dto データオブジェクト
	 * @return なし
	 */
	public void delete(TScheduleDto dto) {
		//******************************************************************************************
		//* DBのレコードを削除する
		//******************************************************************************************
		Map<String, Object> conditionMap = new HashMap<String, Object>(); // 抽出条件マップ
		conditionMap.put(TScheduleDto.COLUMN_ID , dto.getId());
		// DBのレコードを削除する
		this.delete(conditionMap);
		// 処理終了
		return;
	}

	/**
	 * DBのレコードを削除します。
	 * @param conditionMap 抽出条件マップ
	 * @return なし
	 */
	public void delete(Map<String, Object> conditionMap) {
		//******************************************************************************************
		//* DBのレコードを削除する
		//******************************************************************************************
		String whereStr = SQLUtils.toWhereString(conditionMap); // 抽出条件文字列
		this.db.delete(TScheduleDto.TABLE_NAME, whereStr, null);
		// 処理終了
		return;
	}

	/**
	 * DBのレコードを抽出します。
	 * @param id データID
	 * @return 抽出データ
	 */
	public TScheduleDto find(Long id) {
		return null;
	}

	/**
	 * DBのレコードを抽出します。
	 * @param conditionMap 抽出条件マップ
	 * @param orderByMap 並び順マップ
	 * @return 抽出データリスト
	 */
	public List<TScheduleDto> find(Map<String, Object> conditionMap, Map<String, Object> orderByMap) {
		//******************************************************************************************
		//* DBのレコードを抽出する
		//******************************************************************************************
		List<TScheduleDto> dtoList = new ArrayList<>(); // 抽出データリスト
		String whereStr   = SQLUtils.toWhereString(conditionMap); // 抽出条件文字列
		String orderByStr = SQLUtils.toOrderByString(orderByMap); // 並び順
		// DBのレコードを抽出する
		Cursor cursor =
 			this.db.query(
				TScheduleDto.TABLE_NAME, null, whereStr, null, null, null,
				orderByStr
			);
		// 抽出対象のレコードが存在する場合
		if (cursor.moveToFirst()) {
			// 抽出対象レコードを全てリストに追加する
			while (!cursor.isAfterLast()) {
				dtoList.add(this.getRecord(cursor));
				cursor.moveToNext();
			}
		}
		// カーソルを破棄する
		cursor.close();

		// 処理終了
		return dtoList;
	}

///// protected method

	/**
	 * カーソル位置のレコードデータを取得します。
	 * @param cursor カーソルオブジェクト
	 * @return レコードデータ
	 */
	protected TScheduleDto getRecord(Cursor cursor) {
		//**************************************************************************************
		//* カーソル位置のレコードデータを取得する
		//**************************************************************************************
		TScheduleDto dto = new TScheduleDto();
		dto.setId(cursor.getLong(cursor.getColumnIndexOrThrow(TScheduleDto.COLUMN_ID)));
		dto.setDate(cursor.getString(cursor.getColumnIndexOrThrow(TScheduleDto.COLUMN_DATE)));
		dto.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(TScheduleDto.COLUMN_TITLE)));
		dto.setContent(cursor.getString(cursor.getColumnIndexOrThrow(TScheduleDto.COLUMN_CONTENT)));
		dto.setCreated(cursor.getString(cursor.getColumnIndexOrThrow(TScheduleDto.COLUMN_CREATED)));
		dto.setModified(cursor.getString(cursor.getColumnIndexOrThrow(TScheduleDto.COLUMN_MODIFIED)));
		// 処理終了
		return dto;
	}

	/**
	 * DTOに格納されているデータをContentValuesに変換します。
	 * @param dto データオブジェクト
	 * @return ContentValues
	 */
	private ContentValues __toContentValues(TScheduleDto dto) {
		ContentValues values = new ContentValues();
		values.put(TScheduleDto.COLUMN_ID, dto.getId());
		values.put(TScheduleDto.COLUMN_DATE, dto.getDate());
		values.put(TScheduleDto.COLUMN_TITLE, dto.getTitle());
		values.put(TScheduleDto.COLUMN_CONTENT, dto.getContent());
		values.put(TScheduleDto.COLUMN_CREATED, dto.getCreated());
		values.put(TScheduleDto.COLUMN_MODIFIED, dto.getModified());
		return values;
	}
}