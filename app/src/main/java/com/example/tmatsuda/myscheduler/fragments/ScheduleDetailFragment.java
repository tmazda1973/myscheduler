package com.example.tmatsuda.myscheduler.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tmatsuda.myscheduler.R;

/**
 * スケジュール情報の詳細表示要素を描画するフラグメントクラスです。
 * @author t.matsuda
 */
public class ScheduleDetailFragment extends Fragment {

///// property

	/**
	 * データID
	 */
	private Long mDataId;

///// method

	/**
	 * 自オブジェクトのインスタンスを生成します。
	 * @param dataId データID
	 * @return 自オブジェクトのインスタンス
	 */
	public static ScheduleDetailFragment newInstance(Long dataId) {
		ScheduleDetailFragment fragment = new ScheduleDetailFragment();
		/*** フラグメントに渡すパラメータ値を設定する ***/
		Bundle args = new Bundle();
		// データID
		if (dataId != null && dataId > 0) {
			args.putLong("dataId", dataId);
			fragment.setArguments(args);
		}
		// 処理終了
		return fragment;
	}

	/**
	 * コンストラクタ
	 */
	public ScheduleDetailFragment() {}

	/**
	 * 親アクティビティが生成された後に呼び出されます。
	 * @param savedInstanceState
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// 親クラスの処理を実行する
		super.onActivityCreated(savedInstanceState);
		// パラメータが指定されている場合
		if (this.getArguments() != null) {
			this.mDataId = this.getArguments().getLong("dataId");
		}
	}

	/**
	 * フラグメントがUIの描画を開始した時に呼び出されます。
	 * @param inflater
	 * @param container
	 * @param savedInstanceState
	 * @return フラグメントのルートビュー
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup root = (ViewGroup)inflater.inflate(R.layout.fragment_schedule_detail, null);
		return  root;
	}
}
