package com.example.tmatsuda.myscheduler.common.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * SQLに関する共通ユーティリティクラスです。
 * @author thinkware
 */
public final class SQLUtils {

	/**
	 * コンストラクタ(インスタンス生成不可)
	 */
	private SQLUtils() {}

	/**
	 * 抽出条件マップを絞り込んで新規にマップを生成します。
	 * - パラメータで指定された抽出条件マップから、絞込み対象のキーと値を抽出して新規にマップを生成します。
	 * @param conditionMap 抽出条件マップ
	 * @param keyValues 絞込み対象となるキーの配列
	 * @return 抽出条件マップ(絞込み後)
	 */
	public static Map<String, Object> narrowConditionMap(
			Map<String, Object> conditionMap, String[] keyValues) {
		//******************************************************************************************
		//* 抽出条件マップを絞り込む
		//******************************************************************************************
		Map<String, Object> narrowCondMap = new HashMap<String, Object>(); // 抽出条件マップ(絞込み用)
		// 抽出条件マップが指定されていない場合は終了する
		if (conditionMap == null) {
			// 処理終了
			return narrowCondMap;
		}
		// 絞込み対象キーが指定されていない場合は終了する
		if (keyValues == null) {
			// 処理終了
			return conditionMap;
		}
		List<String> narrowKeyList = Arrays.asList(keyValues); // 絞込み対象キーリスト
		// 抽出条件マップのキー件数分ループする
		for (String key : conditionMap.keySet()) {
			// 絞込み対象のキーである場合
			if (narrowKeyList.contains(key)) {
				// 抽出条件値を絞込み用のマップに登録する
				narrowCondMap.put(key, conditionMap.get(key));
			}
		}

		// 処理終了
		return narrowCondMap;
	}

	/**
	 * 抽出条件をWHERE句文字列に変換します。
	 * @param conditionMap 抽出条件マップ
	 * @return WHERE句文字列
	 */
	public static String toWhereString(Map<String, Object> conditionMap) {
		//******************************************************************************************
		//* 抽出条件をWHERE句文字列に変換する
		//******************************************************************************************
		StringBuilder wherePhrBuilder = new StringBuilder(); // WHERE句文字列
		Set<String> conditionKeySet = conditionMap.keySet(); // 抽出条件のキーセット
		// 抽出条件のキーセット数分ループする
		for (String key : conditionKeySet) {
			// WHERE句文字列が既に設定されている場合
			if (wherePhrBuilder.length() > 0) {
				// 区切り文字を連結する
				wherePhrBuilder.append(" AND ");
			}
			// 抽出条件を連結する
			wherePhrBuilder.append(key + "=" + String.valueOf(conditionMap.get(key)));
		}
		// 処理終了
		return wherePhrBuilder.toString();
	}

	/**
	 * 抽出条件をWHERE句文字列(プレースホルダ形式)に変換します。
	 * @param conditionMap 抽出条件マップ
	 * @return WHERE句文字列
	 */
	public static String toWhereStringPlaceholder(Map<String, Object> conditionMap) {
		//******************************************************************************************
		//* 抽出条件をWHERE句文字列に変換する
		//******************************************************************************************
		StringBuilder wherePhrBuilder = new StringBuilder(); // WHERE句文字列
		Set<String> conditionKeySet = conditionMap.keySet(); // 抽出条件のキーセット
		// 抽出条件のキーセット数分ループする
		for (String key : conditionKeySet) {
			// WHERE句文字列が既に設定されている場合
			if (wherePhrBuilder.length() > 0) {
				// 区切り文字を連結する
				wherePhrBuilder.append(" AND ");
			}
			// 抽出条件を連結する
			wherePhrBuilder.append(key + "=?");
		}
		// 処理終了
		return wherePhrBuilder.toString();
	}

	/**
	 * 並び順をORDER BY句文字列に変換します。
	 * @param orderbyMap 抽出条件マップ
	 * @return ORDER BY句文字列
	 */
	public static String toOrderByString(Map<String, Object> orderByMap) {
		//******************************************************************************************
		//* 並び順をORDER BY句文字列に変換する
		//******************************************************************************************
		StringBuilder phrBuilder = new StringBuilder(); // ORDER BY句文字列
		Set<String> keySet = orderByMap.keySet(); // 抽出条件のキーセット
		// 抽出条件のキーセット数分ループする
		for (String key : keySet) {
			// WHERE句文字列が既に設定されている場合
			if (phrBuilder.length() > 0) {
				// 区切り文字を連結する
				phrBuilder.append(" , ");
			}
			// 抽出条件を連結する
			phrBuilder.append(key + " " + String.valueOf(orderByMap.get(key)));
		}
		// 処理終了
		return phrBuilder.toString();
	}
}
