package com.example.tmatsuda.myscheduler.helper.db;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;

import com.example.tmatsuda.myscheduler.common.utils.IOUtils;
import com.example.tmatsuda.myscheduler.greendao.DaoMaster;
import com.example.tmatsuda.myscheduler.greendao.DaoSession;

import java.io.IOException;

/**
 * GreenDAO用のDB操作ヘルパークラスです。
 * @author t.matsuda
 */
public class GreenDaoDbHelper extends DaoMaster.OpenHelper {

	private static final String TAG = GreenDaoDbHelper.class.getSimpleName();

	/** DB名 */
	private static final String DB_NAME = "myscheduler_db.sqlite";
	/** SQLiteDatabaseのインスタンス */
	private SQLiteDatabase mDb = null;
	/** DaoMasterのインスタンス */
	private DaoMaster mDaoMaster = null;
	/** DaoSessionのインスタンス */
	private DaoSession mDaoSession = null;
	/** コンテキスト、DBのマイニング用 */
	private Context mContext = null;
	/** SQLOpenHelperのインスタンス */
	private static GreenDaoDbHelper mInstance;

	/**
	 * 最初にインスタンス化する際に使用します。
	 * @param context コンテキスト
	 * @param factory
	 * @return 自オブジェクトのインスタンス
	 */
	public static synchronized GreenDaoDbHelper getInstance(
			Context context,  SQLiteDatabase.CursorFactory factory) {
		if (GreenDaoDbHelper.mInstance == null) {
			GreenDaoDbHelper.mInstance = new GreenDaoDbHelper(context, factory);
		}
		return GreenDaoDbHelper.mInstance;
	}

	/**
	 * 二回目以降の取得はこのメソッドを使用します。
	 * @return 自オブジェクトのインスタンス
	 * @throws RuntimeException
	 */
	public static GreenDaoDbHelper getInstance() {
		if (GreenDaoDbHelper.mInstance == null) {
			throw new RuntimeException();
		}
		return GreenDaoDbHelper.mInstance;
	}

	/**
	 * コンストラクタ
	 * @param context
	 * @param factory
	 */
	private GreenDaoDbHelper(Context context, SQLiteDatabase.CursorFactory factory) {
		super(context, DB_NAME, factory);
		this.mContext = context;
	}

	/**
	 * 初期化メソッド、DBの最初の作成時に一度だけ呼ばれます。
	 * @param db
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		super.onCreate(db);
		try {
			this.__execSql(db,"sql/create");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * データベースをバージョンアップした時に呼ばれます。
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		try {
			// SQLを実行する(DROP TABLE)
			this.__execSql(db, "sql/drop");
		}
		// 例外が発生した場合：[一般例外]
		catch (IOException e) {
			e.printStackTrace();
		}
		// DBを作成する
		this.onCreate(db);
	}

	/**
	 * SQLiteDatabaseのinstanceを返します。
	 * @return DBオブジェクトのインスタンス
	 */
	public SQLiteDatabase getDatabase() {
		if (this.mDb == null) {
			this.mDb = this.getWritableDatabase();
		}
		return this.mDb;
	}

	/**
	 * データベースをクローズします。
	 */
	public void closeDatabase() {
		if (this.mDb != null) {
			this.mDb.close();
			this.mDb = null;
		}
	}

	/**
	 * GreenDaoのDaoMasterのinstanceを返します。
	 * @return DaoMasterオブジェクトのインスタンス
	 */
	public DaoMaster getDaoMaster() {
		if (this.mDaoMaster == null) {
			this.mDaoMaster = new DaoMaster(this.getDatabase());
		}
		return this.mDaoMaster;
	}

	/**
	 * GreenDaoのDaoSessionのinstanceを返します
	 * @return DaoSessionオブジェクトのインスタンス
	 */
	public DaoSession getDaoSession() {
		if (this.mDaoSession == null) {
			this.mDaoSession = this.getDaoMaster().newSession();
		}
		return this.mDaoSession;
	}

	/**
	 * トランザクションを開始します。
	 */
	public void begin() {
		this.getDatabase().beginTransaction();
	}

	/**
	 * トランザクションをコミットします。
	 */
	public void commit() {
		this.getDatabase().setTransactionSuccessful();
	}

	/**
	 * トランザクションを終了します。
	 */
	public void endTransaction() {
		this.getDatabase().endTransaction();
	}

///// private method

	/**
	 * assetsフォルダ内のSQLファイルを実行します。<br />
	 * ファイル内に複数のSQL文を記述する場合、区切り文字("@@")でSQL文を分割します。
	 * @param db データベース
	 * @param assetsDir assetsフォルダ内のフォルダのパス
	 * @throws IOException
	 */
	private void __execSql(SQLiteDatabase db, String assetsDir)
			throws IOException {
		//******************************************************************************************
		//* assetsフォルダ内のSQLファイルを実行する
		//******************************************************************************************
		AssetManager as = this.mContext.getResources().getAssets();
		try {
			// assetsフォルダ配下のファイルを取得する
			String files[] = as.list(assetsDir);
			// ファイルの件数分ループする
			for (String file: files) {
				// SQLファイルを読み込む
				String sql_str = IOUtils.readFile(as.open(assetsDir + "/" + file), "UTF-8");
				// 区切り文字("@@")で分割した件数分ループする
				for (String sql : sql_str.split("@@")) {
					// SQL文字列が存在する場合
					if (sql != null && sql.length() > 0) {
						// SQLを実行する
						db.execSQL(sql);
					}
				}
			}
		}
		// 例外が発生した場合：[入出力例外]
		catch (IOException ioe) {
			ioe.printStackTrace();
			throw ioe;
		}
		// 例外が発生した場合：[一般例外]
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
