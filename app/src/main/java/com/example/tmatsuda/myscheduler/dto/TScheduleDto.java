package com.example.tmatsuda.myscheduler.dto;

import java.io.Serializable;

/**
 * DBレコードのDTOクラスです。（スケジュールテーブル）
 * @author thinkware
 */
@SuppressWarnings("serial")
public class TScheduleDto implements Serializable {

///// const

	//**********************************************************************************************
	//* 定数
	//**********************************************************************************************
	/** テーブル名 */
	public static final String TABLE_NAME = "t_schedule";

	/** カラム名：スケジュールID */
	public static final String COLUMN_ID = "id";
	/** カラム名：日付 */
	public static final String COLUMN_DATE = "date";
	/** カラム名：タイトル */
	public static final String COLUMN_TITLE = "title";
	/** カラム名：内容 */
	public static final String COLUMN_CONTENT = "content";
	/** カラム名：登録日時 */
	public static final String COLUMN_CREATED = "created";
	/** カラム名：更新日時 */
	public static final String COLUMN_MODIFIED = "modified";

///// field

	/** 項目値：ID Integer */
	private Long id = null;
	/** 項目値：日付 text */
	private String date = null;
	/** 項目値：タイトル text */
	private String title = null;
	/** 項目値：内容 text */
	private String content = null;
	/** 項目値：登録日時 string */
	private String created = null;
	/** 項目値：更新日時 string */
	private String modified = null;

///// accessor

	/**
	 * IDを取得します。
	 * @return ID
	 */
	public Long getId() {
		return this.id;
	}
	/**
	 * IDを設定します。
	 * @param id ID
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * スケジュール日付を取得します。
	 * @return スケジュール日付
	 */
	public String getDate() {
		return this.date;
	}
	/**
	 * スケジュール日付を設定します。
	 * @param date スケジュール日付
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * タイトルを取得します。
	 * @return タイトル
	 */
	public String getTitle() {
		return this.title;
	}
	/**
	 * タイトルを設定します。
	 * @param title タイトル
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 内容を取得します。
	 * @return 内容
	 */
	public String getContent() {
		return this.content;
	}
	/**
	 * 内容を設定します。
	 * @param content 内容
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * 登録日時を取得します。
	 * @return 登録日時
	 */
	public String getCreated() {
		return this.created;
	}
	/**
	 * 登録日時を設定します。
	 * @param created 登録日時
	 */
	public void setCreated(String created) {
		this.created = created;
	}

	/**
	 * 更新日時を取得します。
	 * @return 更新日時
	 */
	public String getModified() {
		return this.modified;
	}
	/**
	 * 更新日時を設定します。
	 * @param modified 更新日時
	 */
	public void setModified(String modified) {
		this.modified = modified;
	}
}