package com.example.tmatsuda.myscheduler.activities;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import com.example.tmatsuda.myscheduler.helper.db.DatabaseHelper;

/**
 * アプリケーションの基底となるアクティビティクラスです。
 * @author t.matsuda
 */
public class BaseActivity extends AppCompatActivity {

///// event

	/**
	 * アクティビティ生成時のイベントメソッドです。<br />
	 * - アクティビティ開始時に1度だけ呼び出されます。
	 * @see AppCompatActivity#onCreate(android.os.Bundle)
	 * @param savedInstanceState バンドルオブジェクト
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	/**
	 * オプションメニュー生成時のイベントメソッドです。<br />
	 * - オプションメニューが最初に呼び出される時に1度だけ呼び出されます。
	 * @see AppCompatActivity#onCreateOptionsMenu(android.view.Menu)
	 * @param menu メニューオブジェクト
	 * @return true / false
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}

///// method

	/**
	 * DBの初期処理を実行します。<br />
	 * - 端末にDBが作成されていない場合、DBを作成します。
	 */
	protected void initDB() {
		//******************************************************************************************
		//* DBの初期処理を実行する
		//******************************************************************************************
		// データベースヘルパーのインスタンスを作成する（まだデータベースはできない）
		DatabaseHelper dbHelper = DatabaseHelper.getInstance(this);
		// データベースオブジェクトを取得する（データベースにアクセスすると作成される）
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		// データベースを閉じる
		db.close();
	}
}
