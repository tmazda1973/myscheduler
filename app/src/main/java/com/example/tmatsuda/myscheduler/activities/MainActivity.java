package com.example.tmatsuda.myscheduler.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.tmatsuda.myscheduler.R;
import com.example.tmatsuda.myscheduler.common.utils.DateUtils;
import com.example.tmatsuda.myscheduler.fragments.ScheduleItemsFragment;
import com.example.tmatsuda.myscheduler.listener.OnCalenderDateChangeListener;

/**
 * アプリケーションのメインアクティビティクラスです。
 * @author t.matsuda
 */
public class MainActivity extends BaseActivity implements OnCalenderDateChangeListener {

///// property

	/**
	 * 基準日
	 */
	private String mCurrentDate;

///// method

	/**
	 * @see BaseActivity#onCreate(Bundle)
	 * @param savedInstanceState バンドルオブジェクト
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_main);
		// 管理下のフラグメントを更新する
		this.mCurrentDate = DateUtils.toDateFormat(DateUtils.FORMAT_YYYYMMDD_HYPHEN); // 基準日
		this.__replaceFragment();
	}

	/**
	 * @see BaseActivity#onCreateOptionsMenu(Menu)
	 * @param menu メニューオブジェクト
	 * @return 処理結果
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	/**
	 * @see BaseActivity#onOptionsItemSelected(MenuItem)
	 * @param item 選択されたメニュー要素
	 * @return 処理結果
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_add_schedule) {
			// スケジュール登録画面を表示する
			Intent intent = new Intent(this, ScheduleFormActivity.class);
			this.startActivity(intent);
			return true;
		}
		else if (id == R.id.action_list_schedule) {
			// スケジュール一覧画面を表示する
			Intent intent = new Intent(this, ScheduleItemsActivity.class);
			this.startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

///// private method

	/**
	 * 管理下のフラグメントを更新します。<br />
	 * - フラグメントにパラメータを渡して再描画処理を行います。
	 */
	private void __replaceFragment() {
		/*** フラグメントにパラメータを渡す ***/
		FragmentManager fragmentManager = this.getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		// スケジュール一覧
		ScheduleItemsFragment fragment = ScheduleItemsFragment.newInstance(this.mCurrentDate);
		transaction.replace(R.id.schedule_items_container, fragment, "schedule_items");
		// トランザクションをコミットする
		transaction.commit();
	}

///// listener

	/**
	 * カレンダー要素の日付が変更された時に呼び出されます。
	 * @param changeDate 変更後の日付文字列（yyyy-MM-dd）
	 */
	public void onDateChanged(String changeDate) {
		// TODO: デバッグ用
		Toast.makeText(this.getApplicationContext(), "[onDateChanged] : " + changeDate, Toast.LENGTH_SHORT).show();
		// 管理下のフラグメントを更新する
		this.mCurrentDate = changeDate; // 基準日
		this.__replaceFragment();
	}
}