package com.example.tmatsuda.myscheduler.fragments;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tmatsuda.myscheduler.R;
import com.example.tmatsuda.myscheduler.adapter.ScheduleAdapter;
import com.example.tmatsuda.myscheduler.common.utils.StringUtils;
import com.example.tmatsuda.myscheduler.dto.TScheduleDto;
import com.example.tmatsuda.myscheduler.model.ScheduleModel;

import java.util.List;

/**
 * スケジュール一覧要素を描画するフラグメントクラスです。
 * @author t.matsuda
 */
public class ScheduleItemsFragment extends ListFragment {

///// property

	/**
	 * リストデータのアダプタークラス
	 */
	private ScheduleAdapter mScheduleAdapter;

///// method

	/**
	 * 自オブジェクトのインスタンスを生成します。
	 * @param date 基準日
	 * @return 自オブジェクトのインスタンス
	 */
	public static ScheduleItemsFragment newInstance(String date) {
		ScheduleItemsFragment fragment = new ScheduleItemsFragment();
		/*** フラグメントに渡すパラメータ値を設定する ***/
		Bundle args = new Bundle();
		// 基準日
		if (!StringUtils.isEmpty(date)) {
			args.putString("date", date);
			fragment.setArguments(args);
		}
		// 処理終了
		return fragment;
	}

	/**
	 * コンストラクタ
	 */
	public ScheduleItemsFragment() {}

	/**
	 * 親アクティビティが生成された後に呼び出されます。
	 * @param savedInstanceState
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// 親クラスの処理を実行する
		super.onActivityCreated(savedInstanceState);
		/*** 基準日以前のスケジュールを取得する ***/
		String date = null; // 基準日
		// パラメータが指定されている場合
		if (this.getArguments() != null) {
			date = this.getArguments().getString("date");
		}
		// スケジュールデータを抽出する
		ScheduleModel model = new ScheduleModel(this.getActivity());
		List<TScheduleDto> items = model.find(date);
		// TODO: ダミーデータを設定する
		TScheduleDto dto = new TScheduleDto();
		dto.setId(Long.valueOf(1));
		dto.setDate("2015/10/04");
		dto.setTitle("test");
		dto.setContent("test_content");
		items.add(dto);
		dto = new TScheduleDto();
		dto.setId(Long.valueOf(2));
		dto.setDate("2015/10/03");
		dto.setTitle("test2");
		dto.setContent("test_content2");
		items.add(dto);
		dto = new TScheduleDto();
		dto.setId(Long.valueOf(3));
		dto.setDate("2015/10/02");
		dto.setTitle("test3");
		dto.setContent("test_content3");
		items.add(dto);
		dto = new TScheduleDto();
		dto.setId(Long.valueOf(4));
		dto.setDate("2015/10/02");
		dto.setTitle("test4");
		dto.setContent("test_content4");
		items.add(dto);
		dto = new TScheduleDto();
		dto.setId(Long.valueOf(5));
		dto.setDate("2015/10/02");
		dto.setTitle("test６");
		dto.setContent("test_content5aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaあああああああああああああああ");
		items.add(dto);
		// アダプターを生成する
		this.mScheduleAdapter = new ScheduleAdapter(this.getActivity(), 0, items);
		this.setListAdapter(this.mScheduleAdapter);
	}

	/**
	 * フラグメントがUIの描画を開始した時に呼び出されます。
	 * @param inflater
	 * @param container
	 * @param savedInstanceState
	 * @return フラグメントのルートビュー
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_schedule_items, container, false);
		return view;
	}
}
