package com.example.tmatsuda.myscheduler.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.example.tmatsuda.myscheduler.R;
import com.example.tmatsuda.myscheduler.common.utils.DateUtils;

/**
 * 日付選択ダイアログのフラグメントクラスです。
 * @author t.matsuda
 */
public class DatePickerDialogFragment extends DialogFragment implements
        DatePickerDialog.OnDateSetListener {

	/**
	 * タグ名
	 */
	@SuppressWarnings("unused")
	private static final String TAG = DatePickerDialogFragment.class.getSimpleName();

///// const

	/** パラメータ名：[年] */
	public static final String ARG_PARAM_YEAR  = "year";
	/** パラメータ名：[月] */
	public static final String ARG_PARAM_MONTH = "month";
	/** パラメータ名：[日] */
	public static final String ARG_PARAM_DAY   = "day";

///// property

	/**
	 * 値を渡すためのリスナーインタフェース
	 */
	private OnFragmentInteractionListener mListener;

///// interface

	/**
	 * 値を渡すためのインターフェースです。
	 */
	public interface OnFragmentInteractionListener {
		public void onFragmentInteraction(int year, int month, int day);
	}

///// method

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// パラメータ値を取り出す
		Bundle args = this.getArguments();
		int year  = args.getInt(ARG_PARAM_YEAR);
		int month = args.getInt(ARG_PARAM_MONTH);
		int day   = args.getInt(ARG_PARAM_DAY);
		// 日付選択ダイアログを生成する
		DatePickerDialog dialog =
		        new DatePickerDialog(this.getActivity(), this, year, month, day);
		// 処理終了
		return dialog;
	}

	/**
	 * 自オブジェクトのインスタンスを生成します。
	 * @param year 年
	 * @param month 月
	 * @param day 日
	 * @return 自オブジェクトのインスタンス
	 */
    public static DatePickerDialogFragment newInstance(int year, int month, int day) {
		DatePickerDialogFragment fragment = new DatePickerDialogFragment();
		/*** パラメータを設定する ***/
		Bundle args = new Bundle();
		args.putInt(ARG_PARAM_YEAR, year);
		args.putInt(ARG_PARAM_MONTH, month);
		args.putInt(ARG_PARAM_DAY, day);
		fragment.setArguments(args);
        return fragment;
    }

	/**
	 * コンストラクタ
	 */
    public DatePickerDialogFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_date_picker_dialog, container, false);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			this.mListener = (OnFragmentInteractionListener)activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
			        + " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		// リスナーインタフェースへの参照を初期化する
		this.mListener = null;
	}

///// implements method

	/**
	 * 日付選択ダイアログの「設定」ボタンが押下された時に呼び出されます。
	 * @param view Viewオブジェクト
	 * @param year 選択日付：年
	 * @param monthOfYear 選択日付：月
	 * @param dayOfMonth 選択日付：日
	 */
	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		// 親アクティビティがインターフェイスを実装していない場合
		if (this.mListener == null) {
			//処理終了
			return;
		}
		// 親アクティビティに値を引き渡す
		this.mListener.onFragmentInteraction(year, monthOfYear, dayOfMonth);
	}

}
