-- スケジュール情報を格納するテーブル
CREATE TABLE t_schedule
(
  schedule_id integer DEFAULT 1 NOT NULL PRIMARY KEY AUTOINCREMENT,
  schedule_date  text NOT NULL,
  title text NOT NULL,
  content text NOT NULL,
  delete_flg integer DEFAULT 0,
  created text,
  modified text
);